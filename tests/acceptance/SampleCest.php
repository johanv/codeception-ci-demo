<?php

namespace Johanv\Tests\Acceptance;

final class SampleCest
{
    public function checkISeeThePage(\WebTester $i)
    {
        $i->amOnPage('/');
        $i->see('Say hello to:');
    }

    public function checkIAddAnItem(\WebTester $i)
    {
        $i->amOnPage('/');
        $i->waitForElement('#itemInput');
        $i->fillField(['id' => 'itemInput'], 'World');
        $i->click('Go');
        $i->see('Hello World!');
    }
}
