# Running codeception web tests with gitlab-ci

I created this demo project because I have struggled a lot to get this working on another project I work on.
Initially I just wanted to run web tests, I added running yarn during CI later on.

## The demo application

The application I will be testing, is a very basic vue.js application. 
Using an outdated version of vue.js. But that doesn't matter here.

If you want to try it out, you first need to run yarn:
```
yarn install
yarn encore dev
```
If you don't have yarn, see the [installation instructions](https://yarnpkg.com/lang/en/docs/install/).

Then you can just open the the `public/index.html` page in your browser.

## Testing locally

To run the tests on your local computer, make sure you have a working [composer](https://getcomposer.org/).

### Install the dependencies for codeception:

```
   # run this command from the project root:
   composer install
```

### Generate codeception actor classes:

```
   ./vendor/bin/codecept build
```

### Start a local webserver

E.g. the built in server of php:

```
   php -S 0.0.0.0:8080 -t public/
```

### Run a selenium server

This is easy with docker:

```
   docker run --net=host selenium/standalone-chrome
```

### Add a line to `/etc/hosts`

```
127.0.0.1 chrome
```

### Run the tests

```
   ./vendor/bin/codecept run acceptance
```

## Testing with gitlab-ci

I created a [`.gitlab-ci`-file](https://gitlab.com/johanv/codeception-ci-demo/blob/master/.gitlab-ci.yml), and this way every time I push to my
repository, a gitlab runner [runs the web tests](https://gitlab.com/johanv/codeception-ci-demo/pipelines). Which is very cool!

It uses a custom apache container, that serves the file in the [/public](/public)
directory of the repository. The [Dockerfile](https://gitlab.com/johanv/codeception-ci-demo/blob/master/docker/apache/Dockerfile) 
of this container is included in the project source.
